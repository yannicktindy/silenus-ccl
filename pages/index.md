---
blocks:
  - name: hero
    baseline: Build a documentation website quickly
    description: |
      Write pages in [Markdown](page:docs/guides/authoring-content), use [Twig templates](https://cecil.app/documentation/templates) and enjoy the power of [Cecil](https://cecil.app).
  - name: cta
    button: docs/introduction/getting-started
    #link: https://github.com/cecilapp/statidocs
---
:::important
_Statidocs_ is still in development. If something that’s not working, please [open an issue on GitHub](https://github.com/Cecilapp/statidocs/issues/new/choose).
:::
